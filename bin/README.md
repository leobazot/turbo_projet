# Turbo Projet
Buand Liam - Del Marco Romain - Bazot Léo

## Clonner le Projet
Tout d'abord créez une clef ssh (si vous n'en avez pas déjà) et l'ajouter la à BitBucket en suivant [ce tuto](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)


Pour clonner le projet tapez la commande suivante ou nom_du_dossier est le nom du dossier (non-existant) dans lequel le projet sera enregistré
```bash
git clone git@bitbucket.org:leobazot/turbo_projet.git [nom_du_dossier]
```

## Syntaxe javadoc

https://www.poftut.com/javadoc-command-tutorial-for-java-documentation/