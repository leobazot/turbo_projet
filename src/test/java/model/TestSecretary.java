package model;

import model.exceptions.AlreadyBoundToParcoursException;
import model.exceptions.NullParcoursException;
import model.exceptions.NullStudentException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestSecretary {
    private User secretaryTest = new Secretary("director1", "pwd1");
    private Student studentTest = new Student("id1", "fstName1", "lstName1");
    private Mention mentionTest = new Mention("mention1");
    private Parcours parcoursTest1 = new Parcours("parcours1", mentionTest);
    private Parcours parcoursTest2 = new Parcours("parcours1", mentionTest);


    @Test
    public void changeStudentParcours() {
        assertThrows(NullStudentException.class,
                () -> { secretaryTest.changeStudentParcours(null, parcoursTest1);}
        );
        assertThrows(NullParcoursException.class,
                () -> { secretaryTest.changeStudentParcours(studentTest, null); }
        );
        assertDoesNotThrow(() -> {
            secretaryTest.changeStudentParcours(studentTest, parcoursTest1);
        });
        assertEquals(parcoursTest1, studentTest.getParcours());
        assertThrows(AlreadyBoundToParcoursException.class, () -> {
            secretaryTest.changeStudentParcours(studentTest, parcoursTest2);
        });
    }
}
