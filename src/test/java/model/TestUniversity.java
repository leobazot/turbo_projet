package model;

import model.exceptions.NullStudentException;
import model.exceptions.StudentNotFoundException;
import model.exceptions.UserLogInException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestUniversity {
    private University university = University.getUniversity();

    @Test
    public void testAll() {
        assertSame(this.university, University.getUniversity());

        /* ----- SetUser ----- */
        Set<User> usersTests = new HashSet<>();
        usersTests.add(new Director("director1", "pwd1"));
        usersTests.add(new Director("director2", "pwd2"));
        usersTests.add(new Director("director3", "pwd3"));
        usersTests.add(new Director("director4", "pwd4"));
        usersTests.add(new Secretary("secretary1", "pwd1"));
        usersTests.add(new Secretary("secretary2", "pwd2"));
        usersTests.add(new Secretary("secretary3", "pwd3"));
        this.university.setUsers(usersTests);

        /* ----- LOGIN ----- */
        assertThrows(UserLogInException.class,
                () -> { university.logIn("director1", "wrongpwd"); }
        );
        assertThrows(UserLogInException.class,
                () -> { university.logIn("wrongUser", "pwd1"); }
        );
        try {
            university.logIn("director1", "pwd1");
            assertTrue(true);
        } catch (UserLogInException e) {
            fail(e);
        }

        /* ----- ADDSTUDENT ----- */
        try {
            for (int i = 1; i <= 10; i++) {
                University.getUniversity().addStudent(new Student("idStudent" + i, "fstName" + i, "lstName" + i));
            }
            assertFalse(University.getUniversity().getStudents().isEmpty());
            assertEquals(University.getUniversity().getStudents().size(), 10);
        } catch (NullStudentException e) {
            fail(e);
        }

        /* ----- SEARCHSTUDENT ----- */
        assertThrows(StudentNotFoundException.class,
                () -> { this.university.searchStudent("wrongSearch"); }
        );

        try {
            this.university.searchStudent("fstName1");
            assertTrue(true);
        } catch (StudentNotFoundException e) {
            fail(e);
        }

    }
}
