package model;

import model.exceptions.NullUEException;

import static org.junit.jupiter.api.Assertions.*;

public class TestUEOuverture {
    private UE ueTest = new UEOuverture("ue1", 0);
    private UE prerequesiteTest = new UEOuverture("prerequesite1", 0);
    private Student studentTest = new Student("id1", "fstName1", "lstName1");

    public TestUEOuverture() {
        try {
            prerequesiteTest.addPrerequesite(ueTest);
        } catch (NullUEException e) {
            fail(e);
        }
    }

    /*
    public void isRegisterable() {
        assertThrows(MissingPrerequesiteException.class,
                () -> { prerequesiteTest.isRegisterable(studentTest); }
        );
        try {
            assertTrue(ueTest.isRegisterable(studentTest));
            // TODO
        } catch (Exception e) {
            fail(e);
        }
    } */
}
