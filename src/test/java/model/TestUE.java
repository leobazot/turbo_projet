package model;

import model.exceptions.NullUEException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestUE {
    private UE ueTest = new UEOuverture("ue1", 0);
    private UE prerequesiteTest = new UEOuverture("prerequesite1", 0);

    @Test
    public void addPrerequesite() {
        assertThrows(NullUEException.class,
                () -> { ueTest.addPrerequesite(null); }
        );
        assertDoesNotThrow(() -> {
            ueTest.addPrerequesite(prerequesiteTest);
        });
    }
}
