package model;

import model.exceptions.MissingPrerequesiteException;
import model.exceptions.NullStudentException;
import model.exceptions.NullUEException;
import model.exceptions.WrongParcoursException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestUser {
    private User userTest = new Director("user1", "pwd1");
    private Student studentTest = new Student("id1", "fstName1", "lstName1");
    private UE ueTestWithoutPre = new UEOuverture("idUEWithoutPre1", 0);
    private UE ueTestWithPre = new UEOuverture("idUEWithPre1", 0);
    private Mention mentionTest = new Mention("mention1");
    private UE ueMentionTest = new UEMention("ueMention1", 0, mentionTest);

    public TestUser() {
        try {
            ueTestWithPre.addPrerequesite(ueTestWithoutPre);
        } catch (NullUEException e) {
            fail(e);
        }
    }

    @Test
    public void changeStudentId() {
        final String newId = "newId1";

        assertThrows(NullStudentException.class,
                () -> { userTest.changeStudentId(null, null); }
        );
        try {
            userTest.changeStudentId(studentTest, newId);
            assertEquals(studentTest.getId(), newId);
        } catch (NullStudentException e) {
            fail(e);
        }
    }

    @Test
    public void changeStudentLstName() {
        final String newLstName = "newLstName1";

        assertThrows(NullStudentException.class,
                () -> { userTest.changeStudentLstName(null, null); }
        );
        try {
            userTest.changeStudentLstName(studentTest, newLstName);
            assertEquals(studentTest.getLstName(), newLstName);
        } catch (NullStudentException e) {
            fail(e);
        }
    }

    @Test
    public void changeStudentFstName() {
        final String newFstName = "newFstName1";

        assertThrows(NullStudentException.class,
                () -> { userTest.changeStudentFstName(null, null); }
        );
        try {
            userTest.changeStudentFstName(studentTest, newFstName);
            assertEquals(studentTest.getFstName(), newFstName);
        } catch (NullStudentException e) {
            fail(e);
        }
    }

    @Test
    public void registerStudentToUE() {
        assertThrows(NullStudentException.class,
                () -> {userTest.registerStudentToUE(null, ueTestWithoutPre);}
        );
        assertThrows(NullUEException.class,
                () -> {userTest.registerStudentToUE(studentTest, null);}
        );
        assertThrows(MissingPrerequesiteException.class,
                () -> { userTest.registerStudentToUE(studentTest, ueTestWithPre); }
        );
        assertThrows(WrongParcoursException.class,
                () -> { userTest.registerStudentToUE(studentTest, ueMentionTest); }
        );
        assertDoesNotThrow(() -> {
            userTest.registerStudentToUE(studentTest, ueTestWithoutPre);
        });
    }

}
