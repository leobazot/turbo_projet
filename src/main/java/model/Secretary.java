package model;

import model.exceptions.AlreadyBoundToParcoursException;
import model.exceptions.NullParcoursException;
import model.exceptions.NullStudentException;

/**
 * This Secretary class define Scretary users
 * wich have limited ability over the application
 * @author Léo Bazot
 */
public class Secretary extends User {
    /**
     * This constructor should only be used while retrieving data from database (csv)
     * or to execute tests
     * @param id secretary's id
     * @param password user's password hash
     */
    public Secretary(String id, String password) {
        super(id, password);
    }

    /**
     * {@inheritDoc}
     * @param student to change parcours
     * @param newParcours new parcours to register the student to
     * @throws NullStudentException {@inheritDoc}
     * @throws NullParcoursException {@inheritDoc}
     * @throws AlreadyBoundToParcoursException if the student is already registered to a parcours (this user is not allowed to change it)
     */
    public void changeStudentParcours(Student student, Parcours newParcours) throws NullStudentException, NullParcoursException, AlreadyBoundToParcoursException {
        if (student == null) {
            throw new NullStudentException();
        }
        if (student.getParcours() != null) {
            throw new AlreadyBoundToParcoursException(student);
        }
        student.setParcours(newParcours);
    }
}
