package model;

import model.exceptions.NullStudentException;
import model.exceptions.StudentNotFoundException;
import model.exceptions.UserLogInException;

import java.time.LocalDate;
import java.util.*;

/**
 * This University class describe and university and the way it's working
 * @author Léo Bazot
 */
public class University {
    private static volatile University instance;
    private final static Object mutex = new Object();

    // should all those be set to static since there will always be only 1 instance of the class ?
    private Set<UnivYear> univYears = new HashSet<>();
    private Set<User> users = new HashSet<>();
    private Set<Student> students = new HashSet<>();
    private Set<Parcours> parcours = new HashSet<>();
    private Set<UE> ue = new HashSet<>();
    private Set<Mention> mentions = new HashSet<>();
    private User connected; // TODO handle when no one is connected
    private UnivYear currentYear = new UnivYear();
    private Semester currentSemester;

    /**
     * Create an University for the current year using current date
     */
    private University() {
        this.currentSemester = Calendar.getInstance().get(Calendar.MONTH) > 6 ? this.currentYear.getEnd() : this.currentYear.getBegin();
    }

    /**
     * Create an University class if it doesn't exist
     * if it exist just return the instance
     * @return unique instance of the University class
     */
    public static University getUniversity() {
        University result = University.instance; // optimised to acces volatile variable only once when instance already exist
        if (result == null) { // first check to improve performance to prevent getting to the synchronised block on most of the cases
            /* syncronised on a static field of the class that won't ever be null to ensure the block is never reached
             * by 2 different instance even if it shoukd'nt be possible */
            synchronized (University.mutex) {
                result = University.instance;
                if (result == null) { // checking if result is null again in a threadsafe environment
                    University.instance = result = new University();
                }
            }
        }
        return result;
    }

    /**
     * Getter for user connected to the app
     * @return the user connected to the app
     */
    public User getConnected() {
        return this.connected;
    }

    /**
     * Getter for all UE available in the university
     * @return a set of all the UE available in the university
     */
    public Set<UE> getUE() {
        return ue;
    }

    /**
     * Getter for current UnivYear
     * @return the current UnivYear occuring
     */
    public UnivYear getCurrentYear() {
        return currentYear;
    }

    /**
     * Getter for current semester
     * @return the current semester occuring
     */
    public Semester getCurrentSemester() {
        return currentSemester;
    }

    /**
     * Getter for all students subscribed to the university
     * @return a set of all student of the university
     */
    public Set<Student> getStudents() {
        return students;
    }

    /**
     * Getter for all students subscribed to the university
     * @return a set of all parcours of the university
     */
    public Set<Parcours> getParcours() {
        return parcours;
    }

    /**
     * Getter for all mentions available in the university
     * @return a set of all mentions available in the university
     */
    public Set<Mention> getMentions() {
        return this.mentions;
    }

    /**
     * Getter for all years saved in the university
     * @return a Set of all the years saved in the app
     */
    public Set<UnivYear> getUnivYears() {
        return univYears;
    }

    public Set<User> getUsers() {
        return users;
    }

    /**
     * Change the set of users of the application
     * @param users users of the app
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /**
     * Change the set of UEs of the application
     * @param ue ue of the app
     */
    public void setUe(Set<UE> ue) {
        this.ue = ue;
    }

    public void setUnivYears(Set<UnivYear> univYears) {
        this.univYears = univYears;
    }

    /**
     * Log an user to the app
     * @param username of the user
     * @param password of the user
     * @throws UserLogInException when user or password is not found
     */
    public void logIn(String username, String password) throws UserLogInException {
        if (this.users.isEmpty()) {
        }
        for (User u : this.users) {
            if (u.getId().equals(username) && u.logIn(password))
                this.connected = u;
        }
        if (this.connected == null) {
            throw new UserLogInException(username);
        }
    }

    /**
     * For every word (seperated by any space character) in the sequence search if a student name looks like it
     * @param search user's research
     * @return a list of the student wich last or first name contain a researched word
     * @throws StudentNotFoundException if none of the student registered matche the research
     */
    public List<Student> searchStudent(String search) throws StudentNotFoundException {
        List<Student> result = new ArrayList<>();
        List<String> searchElements = new ArrayList<>(Arrays.asList(search.split("\\s")));
        for (String element : searchElements) {
            for (Student student : this.students) {
                if (student.getFstName().equals(element) || student.getLstName().equals(element)) {
                    result.add(student);
                }
            }
        }
        if (result.isEmpty()) {
            throw new StudentNotFoundException(search);
        }
        return result;
    }

    /**
     * Add a student to the list of existing student
     * @param newStudent to add to the list
     * @throws NullStudentException if student is null
     */
    public void addStudent(Student newStudent) throws NullStudentException {
        if (newStudent == null) {
            throw new NullStudentException();
        }
        this.students.add(newStudent);
    }

    /**
     * Set the current year
     * @param y current year
     */
    public void setCurrentYear(UnivYear y) {
        this.univYears.add(y);
        this.currentYear = y;
        this.currentSemester = Calendar.getInstance().get(Calendar.MONTH) > 6 ? this.currentYear.getEnd() : this.currentYear.getBegin();
    }

    /**
     * Set list of parcours available in the university
     * @param parcours available in the university
     */
    public void setParcours(Set<Parcours> parcours) {
        this.parcours = parcours;
    }

    /**
     * Set list of mentions available in the university
     * @param mentions available in the university
     */
    public void setMentions(Set<Mention> mentions) {
        this.mentions = mentions;
    }
}
