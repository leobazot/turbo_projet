package model;

import model.exceptions.MissingPrerequesiteException;
import model.exceptions.NullUEException;
import model.exceptions.WrongParcoursException;

import java.util.*;

/**
 * This UE class represent a generic UE
 * by its id and number of ects given upon succes
 * @author Léo Bazot
 */
public abstract class UE {
    private String id;
    private int ects;
    private Set<UE> prerequesites = new HashSet<>();

    /**
     * Create and UE given its id and ect
     * @param id of the UE
     * @param ects given upon succes
     */
    public UE(String id, int ects) {
        this.id = id;
        this.ects = ects;
    }

    /**
     * Getter for UE's id
     * @return The id of the UE
     */
    public String getId() {
        return id;
    }

    /**
     * Getter for UE's ects given upon success
     * @return the ects given by the UE on succes
     */
    public int getEcts() {
        return ects;
    }

    /**
     * Getter for UE's prerequesites
     * @return the prerequesite UE wich need to be validated to be registered on this UE
     */
    public Set<UE> getPrerequesites() {
        return prerequesites;
    }

    /**
     * Add an UE as prerequesite
     * @param prerequisite new prerequesite for the UE
     * @throws NullUEException
     */
    public void addPrerequesite(UE prerequisite) throws NullUEException {
        if (prerequisite == null) {
            throw new NullUEException();
        } else if (prerequisite == this) {
            // TODO throw an error (an UE can't be it's own prerequesite)
        }
        this.prerequesites.add(prerequisite);
    }

    /**
     * Check if a student can be registered to this UE
     * @param student Student to check
     * @return
     *      - true if the student can be registered to the UE
     *      - false if the Student can't be registered to the UE
     * @throws MissingPrerequesiteException If student is missing a prerequesite to be registered to the UE
     * @throws WrongParcoursException if student is doesn't belong to the good parcours
     */
    public abstract boolean isRegisterable(Student student) throws MissingPrerequesiteException, WrongParcoursException;

    /**
     * Getter for Mention linked to the UE
     * @return the mention linked to the UE
     *         or null if the UE is an UEOuverture
     */
    public abstract Mention getMention();
    
    /**
     * give specific format to print the object in tables for example
     */
    public String toString() {
    	return this.getId();
    }
}
