package model;

import model.exceptions.MissingPrerequesiteException;
import model.exceptions.WrongParcoursException;

public class UEOuverture extends UE {
    /**
     * Create and UE given its id and ect
     * @param id   of the UE
     * @param ects given upon succes
     */
    public UEOuverture(String id, int ects) {
        super(id, ects);
    }

    /**
     * check if the student can register to the UE
     * @param student Student to check
     * @return true if the student can be registered
     * @throws MissingPrerequesiteException if the student is missing a preriquisite to be registered to the ue
     */
    @Override
    public boolean isRegisterable(Student student) throws MissingPrerequesiteException {
        if (!student.getValidateddUE().containsAll(this.getPrerequesites())){
            throw new MissingPrerequesiteException(student, this);
        }
        return true;
    }

    /**
     * Return null because this type of UE can't be linked to a mention
     * @return null
     */
    @Override
    public Mention getMention() {
        return null;
    }
}
