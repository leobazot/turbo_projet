package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This UnivYear class describe a normal university year
 * composed of 2 semester
 * @author Léo Bazot
 */
public class UnivYear {
    private OddSemester begin;
    private EvenSemester end;

    /**
     * Create an UnivYear instance given first its semester
     * @param begin first Semester of this university year
     * @param end second semester of this university year
     */
    public UnivYear(Semester begin, Semester end) {
        this.begin = (OddSemester) begin;
        this.end = (EvenSemester) end;
    }

    /**
     * Create an UnivYear instance given begining and end date of first and second semester
     * @param oddBegining begining Date of the first semester
     * @param oddEnd ending Date of the first semester
     * @param evenBegin begining Date of the second semester
     * @param evenEnd ending Date of the second semester
     */
    public UnivYear(Date oddBegining, Date oddEnd, Date evenBegin, Date evenEnd) {
        this(new OddSemester(oddBegining, oddEnd), new EvenSemester(evenBegin, evenEnd));
    }

    /**
     * Create an UnivYear based on current date given by Calendar java class
     */
    public UnivYear() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        Date beginS1, endS1, beginS2, endS2;
        try {
            beginS1 = format.parse("02/" + "09/" + (cal.get(Calendar.MONTH) > 6 ? year : year - 1));
            endS1 = format.parse("20/" + "12/" + (cal.get(Calendar.MONTH) > 6 ? year : year - 1));
            beginS2 = format.parse("04/" + "01/" + (cal.get(Calendar.MONTH) > 6 ? year + 1 : year));
            endS2 = format.parse("10/" + "06/" + (cal.get(Calendar.MONTH) > 6 ? year + 1 : year));
            this.begin = new OddSemester(beginS1, endS1);
            this.end = new EvenSemester(beginS2, endS2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for this year's first semester
     * @return first semester of this year
     */
    public OddSemester getBegin() {
        return begin;
    }

    /**
     * Getter for this year's second semester
     * @return second semester of this year
     */
    public EvenSemester getEnd() {
        return end;
    }

    /**
     * Return a String representing an UnivYear base on format beginYear/endYear
     * with year in format YYYY
     * @return a String representing an UnivYear base on format beginYear/endYear
     */
    @Override
    public String toString() {
        return this.begin.getYear() + "/" + this.end.getYear();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof UnivYear)) {
            return false;
        }
        UnivYear toCompare = (UnivYear) obj;
        return this.begin.equals(toCompare.begin) && this.end.equals(toCompare.end);
    }
}
