package model.tracking;

import model.Semester;
import model.Student;
import model.UE;
import model.UnivYear;

/**
 * Class to track UE of a student during his scolarship
 */
public class UETracking implements Tracker {
    private Student student;
    private UE ue;
    private Semester semester;
    private UnivYear year;
    private State state;

    /**
     * Create an UE Tracking class
     * @param student to track
     * @param ue to track
     * @param semester when the student has this ue
     * @param year when the student has this ue
     */
    public UETracking(Student student, UE ue, Semester semester, UnivYear year) {
        this.student = student;
        this.ue = ue;
        this.semester = semester;
        this.state = new OnGoingState();
        this.year = year;
    }

    /**
     * getter for tracker's ue
     * @return the ue tracked
     */
    public UE getUe() {
        return ue;
    }

    /**
     * Getter for Tracker's state
     * @return the state of the tracker
     */
    public StateEnum getState() {
        return state.getState();
    }

    public void changeState(State state) {
        this.state = state;
    }

    /**
     * Getter for Tracker's semester
     * @return the semester when the student has this ue
     */
    public Semester getSemester() {
        return semester;
    }

    /**
     * Getter for Tracker's year
     * @return the year when the student has this ue
     */
    public UnivYear getYear() {
        return year;
    }
}
