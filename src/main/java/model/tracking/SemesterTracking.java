package model.tracking;

import model.Semester;
import model.Student;
import model.UE;

/**
 * Track semesters
 * NOT IMPLEMENTED
 */
public class SemesterTracking {
    private Student student;
    private Semester semester;
    private State state;

    public SemesterTracking(Student student, UE ue, Semester semester) {
        this.student = student;
        this.semester = semester;
        this.state = new OnGoingState();
    }

    public Semester getSemester() {
        return this.semester;
    }

    public StateEnum getState() {
        return state.getState();
    }

    public void changeState(State state) {
        this.state = state;
    }
}
