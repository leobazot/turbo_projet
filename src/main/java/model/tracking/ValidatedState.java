package model.tracking;

public class ValidatedState implements State {

    /**
     * {@inheritDoc}
     * @param t tracker to validate
     */
    @Override
    public void validate(Tracker t) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @param t tracker to register
     */
    @Override
    public void register(Tracker t) {
        t.changeState(new OnGoingState());
    }

    /**
     * {@inheritDoc}
     * @param t tracker to fail
     */
    @Override
    public void fail(Tracker t) {
        t.changeState(new FailedState());
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public StateEnum getState() {
        return StateEnum.VALIDATED;
    }
}
