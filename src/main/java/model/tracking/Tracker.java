package model.tracking;

/**
 * Interface for tracker classes
 */
public interface Tracker {
    /**
     * change state of the tracker
     * @param state new state of the tracker
     */
    void changeState(State state);
}
