package model.tracking;

/**
 * List of possible states
 */
public enum StateEnum {
    VALIDATED,
    ONGOING,
    FAILED
}
