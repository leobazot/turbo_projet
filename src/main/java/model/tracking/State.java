package model.tracking;

/**
 * Define what a state can do
 */
public interface State {
    /**
     * Validate t
     * @param t tracker to validate
     */
    void validate(Tracker t);

    /**
     * register t
     * @param t tracker to register
     */
    void register(Tracker t);

    /**
     * fail t
     * @param t tracker to fail
     */
    void fail(Tracker t);

    /**
     * getter for state of the state
     * @return state of the object
     */
    StateEnum getState();
}
