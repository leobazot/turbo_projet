package model.tracking;

/**
 * State when on going
 */
public class OnGoingState implements State {
    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void validate(Tracker tracker) {
        tracker.changeState(new ValidatedState());
    }

    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void register(Tracker tracker) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void fail(Tracker tracker) {
        tracker.changeState(new FailedState());
    }

    /**
     * {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public StateEnum getState() {
        return StateEnum.ONGOING;
    }
}
