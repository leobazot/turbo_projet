package model.tracking;

/**
 * State when failed
 */
public class FailedState implements State {
    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void validate(Tracker tracker) {
        tracker.changeState(new ValidatedState());
    }

    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void register(Tracker tracker) {
        tracker.changeState(new OnGoingState());
    }

    /**
     * {@inheritDoc}
     * @param tracker {@inheritDoc}
     */
    @Override
    public void fail(Tracker tracker) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public StateEnum getState() {
        return StateEnum.FAILED;
    }
}
