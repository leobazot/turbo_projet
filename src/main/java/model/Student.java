package model;

import model.exceptions.MissingPrerequesiteException;
import model.exceptions.NullParcoursException;
import model.exceptions.WrongParcoursException;
import model.tracking.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This Student class represent a student data
 * by his/her id, names and parcours
 * @author Léo Bazot
 */
public class Student {
    private String id;
    private String fstName;
    private String lstName;
    private Parcours parcours;
    private List<UETracking> ueTracking = new ArrayList<>();

    /**
     * This constructor should only be used while retrieving data from database (csv)
     * or to execute tests
     * @param id student's id
     * @param fstName student's first name
     * @param lstName student's last name
     * @param parcours student's parcours
     */
    public Student(String id, String fstName, String lstName, Parcours parcours) {
        this.id = id;
        this.fstName = fstName;
        this.lstName = lstName;
        this.parcours = parcours;
    }

    /**
     * Create a Student without parcours
     * @param id of the student
     * @param fstName student's first name
     * @param lstName student's last name
     */
    public Student(String id, String fstName, String lstName) {
        this(id, fstName, lstName, null);
    }

    /**
     * Getter for student's id
     * @return id of the person
     */
    public String getId() {
        return id;
    }

    /**
     * Getter for student's first name
     * @return first name of the person
     */
    public String getFstName() {
        return fstName;
    }

    /**
     * Getter for student's last name
     * @return last name of the person
     */
    public String getLstName() {
        return lstName;
    }

    /**
     * Getter for student's Parcours
     * @return the Parcours of the student
     */
    public Parcours getParcours() {
        return parcours;
    }

    /**
     * Getter for student's UETracking
     */
    public List<UETracking> getUeTracking() {
        return ueTracking;
    }

    /**
     * Get ue followed by the student
     * @return Set of the UE the student is following
     */
    public Set<UE> getFollowedUE() {
        Set<UE> followedUE = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.ONGOING) {
                followedUE.add(trackElem.getUe());
            }
        }
        return followedUE;
    }

    /**
     * Get ue followed by the student
     * @return Set of the UE the student is following
     */
    public Set<UETracking> getFollowedUETracking() {
        Set<UETracking> followedUE = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.ONGOING) {
                followedUE.add(trackElem);
            }
        }
        return followedUE;
    }

    /**
     * Get ue validated by this student
     * @return Set of the UE the student has validated
     */
    public Set<UE> getValidateddUE() {
        Set<UE> validated = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.VALIDATED) {
                validated.add(trackElem.getUe());
            }
        }
        return validated;
    }

    /**
     * Get ue validated by this student
     * @return Set of the UE the student has validated
     */
    public Set<UETracking> getValidateddUETracking() {
        Set<UETracking> validated = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.VALIDATED) {
                validated.add(trackElem);
            }
        }
        return validated;
    }

    /**
     * Get ue the student can follow
     * @return Set of the UE the student can follow
     */
    public Set<UE> getFollowableUE() {
        Set<UE> followable = new HashSet<>();
        Set<UE> validated = this.getValidateddUE();
        Set<UE> followed = this.getFollowedUE();
        for (UE ue : University.getUniversity().getUE()) {
            if (validated.containsAll(ue.getPrerequesites()) && !validated.contains(ue) && !followed.contains(ue)) { // could use hasPrerequesites function but would be less optimised because ou getValidatedUE call
                followable.add(ue);
            }
        }
        return followable;
    }

    /**
     * Get ue the student has failed
     */
    public Set<UE> getFailedUE() {
        Set<UE> failed = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.FAILED) {
                failed.add(trackElem.getUe());
            }
        }
        return failed;
    }

    /**
     * Get ue the student has failed
     */
    public Set<UETracking> getFailedUETracking() {
        Set<UETracking> failed = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            if (trackElem.getState() == StateEnum.FAILED) {
                failed.add(trackElem);
            }
        }
        return failed;
    }

    /**
     * Get all the ue the student is linked to
     * @return
     */
    public Set<UE> getAllUe() {
        Set<UE> tracking = new HashSet<>();
        for (UETracking trackElem : this.ueTracking) {
            tracking.add(trackElem.getUe());
        }
        return tracking;
    }


    /**
     * Change Student's id
     * @param id new id of the student
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Change student's first name
     * @param fstName new first name of the student
     */
    public void setFstName(String fstName) {
        this.fstName = fstName;
    }

    /**
     * Change student's last name
     * @param lstName new last name of the student
     */
    public void setLstName(String lstName) {
        this.lstName = lstName;
    }

    /**
     * Change student's parcours
     * @param parcours new parcours of the student
     * @throws NullParcoursException if the given parcours is null
     */
    public void setParcours(Parcours parcours) throws NullParcoursException {
        if (parcours == null) {
            throw new NullParcoursException();
        }
        this.parcours = parcours;
    }

    /**
     * Register a student to an UE
     * @param ue the ue the student need to be registered to
     * @throws MissingPrerequesiteException if the student is missing a prerequesite
     * @throws WrongParcoursException if the student is not is the right parcours to be registered to this UE
     */
    public void addUE(UE ue) throws MissingPrerequesiteException, WrongParcoursException {
        if (ue.isRegisterable(this)) {
            this.ueTracking.add(new UETracking(this, ue, University.getUniversity().getCurrentSemester(), University.getUniversity().getCurrentYear()));
        }
    }

    /**
     * Register a student to an UE
     * @param ue the ue the student need to be registered to
     * @throws MissingPrerequesiteException if the student is missing a prerequesite
     * @throws WrongParcoursException if the student is not is the right parcours to be registered to this UE
     */
    public void addUE(UE ue, Semester semester) throws MissingPrerequesiteException, WrongParcoursException {
        UnivYear year = University.getUniversity().getCurrentYear();
        for (UnivYear y : University.getUniversity().getUnivYears()) {
            if (y.getBegin().equals(semester) || y.getEnd().equals(semester)) {
                year = y;
                break;
            }
        }
        if (ue.isRegisterable(this)) {
            this.ueTracking.add(new UETracking(this, ue, semester, year));
        }
    }

    /**
     * Validate an ue for this student
     * @param ue that need to be validated
     */
    public void validateUE(UE ue) {
        for (UETracking ueTrack : this.ueTracking) {
            if (ueTrack.getUe() == ue) {
                ueTrack.changeState(new ValidatedState());
            }
        }
    }

    /**
     * Set an ue as failed for this student
     * @param ue wich have been failed
     */
    public void failUE(UE ue) {
        for (UETracking ueTrack : this.ueTracking) {
            if (ueTrack.getUe() == ue) {
                ueTrack.changeState(new FailedState());
            }
        }
    }

    /**
     * Return status of the student parcours by the nulber of ects he have from his validated ue
     * @return the number of ects the student validated
     */
    public int parcoursStatus() {
        int ects = 0;
        for (UETracking ueT : this.ueTracking) {
            if (ueT.getState() == StateEnum.VALIDATED) {
                ects += ueT.getUe().getEcts();
            }
        }
        return ects;
    }
}