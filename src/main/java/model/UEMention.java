package model;

import model.exceptions.MissingPrerequesiteException;
import model.exceptions.WrongParcoursException;

/**
 * This UEMention class represent an UE linked to a mention
 * by its id, the mention associated and the number of ects given upon succes
 * @author Léo Bazot
 */
public class UEMention extends UE {
    private Mention mention;

    /**
     * Create and UEMention given its id, ect and Mention
     * @param id of the UE
     * @param ects given upon succes
     */
    public UEMention(String id, int ects, Mention mention) {
        super(id, ects);
        this.mention = mention;
    }

    /**
     * Getter for UEMention's linked mention
     * @return the mention this ue is linked to
     */
    public Mention getMention() {
        return mention;
    }

    /**
     * check if the student can register to the UE
     * @param student Student to check
     * @return true if the student can be registered
     * @throws MissingPrerequesiteException if the student is missing a preriquisite to be registered to the ue
     * @throws WrongParcoursException if the student is not is the good parcour to be registered to the ue
     */
    @Override
    public boolean isRegisterable(Student student) throws MissingPrerequesiteException, WrongParcoursException {
        if (!student.getValidateddUE().containsAll(this.getPrerequesites())){
            throw new MissingPrerequesiteException(student, this);
        }
        if (student.getParcours() == null || !this.mention.equals(student.getParcours().getMention())) {
            throw new WrongParcoursException(student, this);
        }
        return true;
    }
}
