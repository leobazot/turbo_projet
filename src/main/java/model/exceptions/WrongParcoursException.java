package model.exceptions;

import model.Student;
import model.UE;
import model.UEMention;

public class WrongParcoursException extends Exception {
    private Student student;
    private UE ue;
    public WrongParcoursException(Student student, UE ue) {
        this.student = student;
        this.ue = ue;
    }

    public String toString() {
        return "Student : " + student.getLstName() + " " + student.getFstName() + " can't be registered to UE "
                + ue.getId() + " because he doesn't belong the parcours which this ue is linked to !";
    }
}
