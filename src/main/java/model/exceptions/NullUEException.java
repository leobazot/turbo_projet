package model.exceptions;

public class NullUEException extends Exception {
    public String toString() {
        return "Given UE is null, please provide a valid UE !";
    }
}
