package model.exceptions;

public class NullStudentException extends Exception {
    public String toString() {
        return "Given student is null, please provide a valid student !";
    }
}
