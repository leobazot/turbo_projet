package model.exceptions;

/**
 * This Exception class describe a connection error
 * @author Léo Bazot
 */
public class UserLogInException extends Exception {
    private String username;
    public UserLogInException(String username) {
        this.username = username;
    }

    public String toString() {
        return "An error occured on " + username + " login attempt !";
    }
}
