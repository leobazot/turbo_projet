package model.exceptions;

public class NullParcoursException extends Exception {
    public String toString() {
        return "Given parcours is null, please provide a valid parcours !";
    }
}
