package model.exceptions;

import model.Student;

public class AlreadyBoundToParcoursException extends Exception {
    private Student student;

    public AlreadyBoundToParcoursException(Student student) {
        this.student = student;
    }

    public String toString() {
        return "Student : " + this.student.getLstName() + " " + this.student.getFstName()
                + "is already registered on Parcours : " + this.student.getParcours().getName();
    }
}
