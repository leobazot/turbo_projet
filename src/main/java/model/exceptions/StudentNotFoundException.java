package model.exceptions;

public class StudentNotFoundException extends Exception {
    private String search;
    public StudentNotFoundException(String search) {
        this.search = search;
    }

    public String toString() {
        return "No result found for \"" + search + "\" research ! (404)";
    }
}
