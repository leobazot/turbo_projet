package model.exceptions;

import model.Student;
import model.UE;

public class MissingPrerequesiteException extends Exception {
    private Student student;
    private UE ue;

    public MissingPrerequesiteException(Student student, UE ue) {
        this.student = student;
        this.ue = ue;
    }

    @Override
    public String toString() {
        return "Student : " + student.getLstName() + " " + student.getFstName() + " can't be registered to UE "
                + ue.getId() + " because he is missing his prerequesites";
    }
}
