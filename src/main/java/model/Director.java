package model;

import model.exceptions.NullParcoursException;
import model.exceptions.NullStudentException;

/**
 * This Director class define Admin users
 * @author Léo Bazot
 */
public class Director extends User {

    /**
     * This constructor should only be used while retrieving data from database (csv)
     * or to execute tests
     * @param id director's id
     * @param password user's password hash
     */
    public Director(String id, String password) {
        super(id, password);
    }

    /**
     * {@inheritDoc}
     * @param student to change parcours
     * @param newParcours new parcours to register the student to
     * @throws NullStudentException {@inheritDoc}
     * @throws NullParcoursException {@inheritDoc}
     */
    public void changeStudentParcours(Student student, Parcours newParcours) throws NullStudentException, NullParcoursException {
        if (student == null) {
            throw new NullStudentException();
        }
        student.setParcours(newParcours);
    }
}













