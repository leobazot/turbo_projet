package model;

/**
 * This Mention class define a mention by its name
 * @author Léo Bazot
 */
public class Mention {
    private String name;

    /**
     * Create a mention given its name
     * @param name mention's name
     */
    public Mention(String name) {
        this.name = name;
    }

    /**
     * Getter for mention's name
     * @return mention's name
     */
    public String getName() {
        return name;
    }

    /**
     * Check if this mention is equal to the object given as parameter
     * @param obj to compare to this
     * @return true if both object have the same name
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Mention))
            return false;
        if (this == obj)
            return true;
        Mention mention = (Mention) obj;
        return this.name.equals(mention.getName());
    }
}
