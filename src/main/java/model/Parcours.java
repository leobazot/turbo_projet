package model;

/**
 * This Parcours class define a parcours
 * by its name and wich mention he subdivide(/belongs to)
 * @author Léo Bazot
 */
public class Parcours {
    private String name;
    private Mention mention;

    /**
     * Create a Parcours given name and assotiated mentions
     * @param name parcours's name
     * @param mention associated mention
     */
    public Parcours(String name, Mention mention) {
        this.name = name;
        this.mention = mention;
    }

    /**
     * Getter for parcour's name
     * @return nale of the parcours
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for parcours's mention
     * @return The mention this porcours belong to
     */
    public Mention getMention() {
        // TODO maybe change to send a copy of the object
        return mention;
    }
    
    /**
     * enables to print a Parcours in a certain format
     */
    @Override
    public String toString() {
        return name;
    }
}
