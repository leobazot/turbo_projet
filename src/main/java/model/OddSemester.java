package model;

import java.util.Date;
import java.util.UUID;

/**
 * This OddSemester represent second(/last) semester of a normal university year
 * @author Léo Bazot
 */
public class OddSemester extends Semester {
    /**
     * Create an OddSemester given begining and ending date
     * @param begining Date when the semester begin
     * @param end Date when the semester end
     */
    public OddSemester(Date begining, Date end) {
        super(begining, end);
    }

    /**
     * Create an OddSemester given begining and ending date
     * @param uuid semester's uuid
     * @param begining Date when the semester begin
     * @param end Date when the semester end
     */
    public OddSemester(UUID uuid, Date begining, Date end) {
        super(uuid, begining, end);
    }
}
