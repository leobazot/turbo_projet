package model;

import java.util.Date;
import java.util.UUID;

/**
 * This EvenSemester represent first semester of a normal university year
 * @author Léo Bazot
 */
public class EvenSemester extends Semester {
    /**
     * Create an EvenSemester given begining and ending date
     * @param begining Date when the semester begin
     * @param end Date when the semester end
     */
    public EvenSemester(Date begining, Date end) {
        super(begining, end);
    }

    /**
     * Create an EvenSemester given an uuid and begining and ending date
     * This should be used by parser only
     * @param uuid semester's uuid
     * @param begining Date when the semester begin
     * @param end Date when the semester end end
     */
    public EvenSemester(UUID uuid, Date begining, Date end) {
        super(uuid, begining, end);
    }
}
