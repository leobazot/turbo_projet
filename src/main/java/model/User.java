package model;

import model.exceptions.*;

/**
 * This User class describe Person wich can connect to the application using passord and their name
 * @author Léo Bazot
 */
public abstract class User {
    private String id;
    private String password;

    /**
     * Create an User given it's id, first name, last name, and password
     * @param id of the user
     * @param password of the user
     */
    public User(String id, String password) {
        this.id = id;
        this.password = password;
    }

    /**
     * Getter for person's id
     * @return id of the person
     */
    public String getId() {
        return id;
    }

    /**
     * Getter for person's password
     * @return password of the person
     */
    public String getPassword() {
        return password;
    }

    /**
     * Change student's id
     * @param student to change id
     * @param newId new id of the student
     * @throws NullStudentException if student parameter is null
     */
    public void changeStudentId(Student student, String newId) throws NullStudentException {
        if (student == null) {
            throw new NullStudentException();
        }
        student.setId(newId);
    }

    /**
     * Change student's last name
     * @param student to change last name
     * @param newLstName new last name of the student
     * @throws NullStudentException if student parameter is null
     */
    public void changeStudentLstName(Student student, String newLstName) throws NullStudentException {
        if (student == null) {
            throw new NullStudentException();
        }
        student.setLstName(newLstName);
    }

    /**
     * Change student's first name
     * @param student to change first name
     * @param newFstName new fistr name of the student
     * @throws NullStudentException if student parameter is null
     */
    public void changeStudentFstName(Student student, String newFstName) throws NullStudentException {
        if (student == null) {
            throw new NullStudentException();
        }
        student.setFstName(newFstName);
    }

    /**
     * Change student's parcours
     * @param student to change parcours
     * @param newParcours new parcours to register the student to
     * @throws NullStudentException if the student parameter is null
     * @throws NullParcoursException if the newParcours parameter is null
     * @throws AlreadyBoundToParcoursException if student already have a parcours he is registered to
     *                                          AND the current user is not allowed to change it
     */
    public abstract void changeStudentParcours(Student student, Parcours newParcours) throws NullStudentException, NullParcoursException, AlreadyBoundToParcoursException;

    /**
     * Register a student to it's ue
     * @param student
     * @param ue
     * @throws NullUEException
     * @throws NullStudentException
     */
    public void registerStudentToUE(Student student, UE ue) throws NullUEException, NullStudentException, MissingPrerequesiteException, WrongParcoursException {
        if (ue == null) {
            throw new NullUEException();
        }
        if (student == null) {
            throw new NullStudentException();
        }
        student.addUE(ue);
    }

    /* ----- PACKAGE METHODS ----- */

    /*
     * Log-in an user
     * @param password user's password
     * @return
     *      - true if given password matche user's password
     *      - false if given password doesn't match user's password
     */
    boolean logIn(String password) {
        // TODO implement password hashing
        return this.password.equals(password);
    }
}
