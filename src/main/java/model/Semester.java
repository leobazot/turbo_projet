package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

/**
 * This Semester class represent an even or odd Semester
 * @author Léo Bazot
 */
public abstract class Semester {
    private UUID id;
    private Date begining;
    private Date end;


    /**
     * Create a Semester given it's begin and end date
     * @param begining Date when the semester begin
     * @param end Date when the semester end
     */
    public Semester(UUID uuid, Date begining, Date end) {
        this.id = uuid;
        this.begining = begining;
        this.end = end;
    }

    /**
     * Create a Semester given it's begin and end date
     * @param begining Date when the semester begin
     * @param end Date when the semester end
     */
    public Semester(Date begining, Date end) {
        this(UUID.randomUUID(), begining, end);
    }

    /**
     * Getter for the id of the semester (used to save them)
     * @return id of the Semester
     */
    public String getId() {
        return id.toString();
    }

    /**
     * Getter for the begining Date of the semester
     * @return Date of the begin of the semester
     */
    public Date getBegining() {
        return (Date) begining.clone();
    }

    /**
     * Getter for the ending Date of the semester
     * @return Date of the end of the semester
     */
    public Date getEnd() {
        return (Date) end.clone();
    }

    /**
     * Getter for the year of the semester
     * @return year when the semester is begining
     */
    public int getYear() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(this.begining);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Get semester on string format as :
     * Du dd/MM/yyyy au dd/MM/yyyy
     * @return semester as a string
     */
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return "Du " + df.format(this.begining) + " au " + df.format(this.end);
    }

    /**
     * Compare the object given in parameter to this
     * @param obj object to compare to this
     * @return true if obj and this have the same id
     *         false if not
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Semester)) {
            return false;
        }
        Semester toCompare = (Semester) obj;
        return this.id.equals(toCompare.id);
    }
}
