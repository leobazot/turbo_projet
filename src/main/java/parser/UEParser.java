package parser;

import model.*;
import model.exceptions.NullUEException;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * Class used to parse UE in ue.csv file
 * format :
 *  ueName,ects,mentionName,perrequesite1:prerequesite2
 */
public class UEParser implements ICsvParser {
    public static final String FILE_NAME = "ue.csv";
    private Set<Mention> mentions;

    public UEParser(Set<Mention> mentions) {
        this.mentions = mentions;
    }

    @Override
    public Set<UE> read() throws IOException, InvalidCsvFormatException, NullUEException {
        Set<UE> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(UEParser.FILE_NAME);
        Map<UE, String> prerequesites = new HashMap<>();

        while (reader.hasNextLine()) {
            String[] line = reader.nextLine().split(",");
            int ects = Integer.parseInt(line[1]);
            UE toAdd = null;
            if (line.length < 3 || line[2].isEmpty()) {
                toAdd = new UEOuverture(line[0], ects);
            } else {
                for (Mention m : this.mentions) {
                    if (m.getName().equals(line[2])) {
                        toAdd = new UEMention(line[0], ects, m);
                        break;
                    }
                }
            }
            result.add(toAdd);
            prerequesites.put(toAdd, line.length == 4 ? line[3] : "");
        }
        for (UE ue : prerequesites.keySet()) {
            String[] preNames = prerequesites.get(ue).split(":");
            for (String preName : preNames) {
                for (UE p : result) {
                    if (preName.equals(p.getId())) {
                        ue.addPrerequesite(p);
                    }
                }
            }
        }
        reader.close();

        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();

        for (UE ue : university.getUE()) {
            if (ue.getMention() == null) {
                toWrite.append(ue.getId()).append(',').append(ue.getEcts()).append(',').append(',');
            } else {
                toWrite.append(ue.getId()).append(',').append(ue.getEcts()).append(',').append(ue.getMention().getName()).append(',');
            }
            for (UE p : ue.getPrerequesites()) {
                toWrite.append(p.getId()).append(':');
            }
            toWrite.append('\n');
        }

        toWrite.deleteCharAt(toWrite.length() - 1);

        writer.write(toWrite.toString());
        writer.close();
    }
}
