package parser;

import model.*;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Class used to parse Mention in mentions.csv file
 * format :
 *  mentionName
 */
public class MentionParser implements ICsvParser {
    public static final String FILE_NAME = "mentions.csv";

    @Override
    public Set<Mention> read() throws IOException {
        Set<Mention> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(MentionParser.FILE_NAME);

        while (reader.hasNextLine()) {
            String mentionName = reader.nextLine();
            result.add(new Mention(mentionName));
        }
        reader.close();

        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();

        for (Mention mention : university.getMentions()) {
            toWrite.append(mention.getName()).append('\n');
        }
        toWrite.deleteCharAt(toWrite.length() - 1);

        writer.write(toWrite.toString());
        writer.close();
    }
}
