package parser;

import model.*;
import model.exceptions.NullUEException;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Class used to parse years (and semesters) in years.csv file
 * format :
 *  idS1,idS2
 */
public class YearParser implements ICsvParser {
    public static final String FILE_NAME = "years.csv";
    private Set<Semester> semesters;

    public YearParser(Set<Semester> semesters) {
        this.semesters = semesters;
    }

    @Override
    public Set<UnivYear> read() throws IOException, InvalidCsvFormatException, NullUEException {
        Set<UnivYear> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(FILE_NAME);
        boolean newYear = true;

        while (reader.hasNextLine()) {
            String[] line = reader.nextLine().split(",");
            Semester s1 = null;
            Semester s2 = null;
            for (Semester s : this.semesters) {
                if (s.getId().equals(line[0])) {
                    s1 = s;
                }
                if (s.getId().equals(line[1])) {
                    s2 = s;
                }
            }
            if (s1 == null || s2 == null) {
                throw new InvalidCsvFormatException("Semester id refers to an unexisting semester");
            }
            UnivYear year = new UnivYear(s1, s2);
            result.add(year);
            if (University.getUniversity().getCurrentYear().equals(year)) {
                University.getUniversity().setCurrentYear(year);
                newYear = false;
            }
        }
        if (newYear) {
            University.getUniversity().getUnivYears().add(University.getUniversity().getCurrentYear());
        }
        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();
        boolean addCurentYear = true;

        for (UnivYear year : university.getUnivYears()) {
            toWrite.append(year.getBegin().getId()).append(',').append(year.getEnd().getId()).append('\n');
            if (year.equals(university.getCurrentYear())) {
                addCurentYear = false;
            }
        }
        if (addCurentYear) {
            toWrite.append(university.getCurrentYear().getBegin().getId()).append(",").append(university.getCurrentYear().getEnd().getId());
        } else {
            toWrite.deleteCharAt(toWrite.length() - 1);
        }

        writer.write(toWrite.toString());
        writer.close();
    }
}
















