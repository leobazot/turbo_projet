package parser;

import model.*;
import model.exceptions.NullStudentException;
import model.exceptions.NullUEException;
import parser.exceptions.InvalidCsvFormatException;

import java.io.*;
import java.util.*;

/**
 * This CsvParser class provide a way to
 * save and read data saveable
 *
 * @author Léo Bazot
 */
public class CsvParser {

    public static Scanner getReader(String fileName) throws IOException {
        File file = new File(CsvParams.SAVE_PATH + fileName);
        file.createNewFile();
        return new Scanner(file);
    }

    public static FileWriter getWriter(String fileName) throws IOException {
        File file = new File(CsvParams.SAVE_PATH + fileName);
        file.createNewFile();
        return new FileWriter(file);
    }

    public static void ReadAll() {
        University university = University.getUniversity();
        try {
            Set<User> users = new UserParser().read();
            Set<Semester> semesters = new SemesterParser().read();
            Set<UnivYear> univYears = new YearParser(semesters).read();
            Set<Mention> mentions = new MentionParser().read();
            Set<UE> ues = new UEParser(mentions).read();
            Set<Parcours> parcours = new ParcoursParser(mentions).read();
            Set<Student> students = new StudentParser(parcours, ues, semesters).read();
            university.setUnivYears(univYears);
            university.setParcours(parcours);
            university.setUsers(users);
            university.setUe(ues);
            university.setMentions(mentions);
            for (Student s : students) {
                university.addStudent(s);
            }
            for (UnivYear y : univYears) {
                if (y.equals(university.getCurrentYear())) {
                    university.setCurrentYear(y);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidCsvFormatException e) {
            e.printStackTrace();
        } catch (NullUEException e) {
            e.printStackTrace();
        } catch (NullStudentException e) {
            e.printStackTrace();
        }
    }

    public static void saveAll() {
        try {
            University university = University.getUniversity();
            Set<Semester> semesters = new HashSet<>();
            for (UnivYear y : university.getUnivYears()) {
                semesters.add(y.getBegin());
                semesters.add(y.getEnd());
            }
            Set<Mention> mentions = new HashSet<>();
            for (Parcours p : university.getParcours()) {
                mentions.add(p.getMention());
            }
            new SemesterParser().write();
            new YearParser(semesters).write();
            new MentionParser().write();
            new UEParser(mentions).write();
            new ParcoursParser(mentions).write();
            new StudentParser(university.getParcours(), university.getUE(), semesters).write();
            new UserParser().write();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




















