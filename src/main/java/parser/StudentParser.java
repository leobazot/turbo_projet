package parser;

import model.*;
import model.tracking.StateEnum;
import model.tracking.UETracking;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Class used to parse Student in students.csv file
 * format :
 *  id,fstName,lstName,parcours,ue1:State:idSemester;ue2:State:idSemester
 *  State must be as :
 *      V = Validater
 *      0 = On going
 *      F = Failed
 */
public class StudentParser implements ICsvParser {
    public static final String FILE_NAME = "students.csv";
    private Set<Parcours> parcoursSet;
    private Set<UE> ue;
    private Set<Semester> semesters;

    public StudentParser(Set<Parcours> parcours, Set<UE> ue, Set<Semester> semesters) {
        this.parcoursSet = parcours;
        this.ue = ue;
        this.semesters = semesters;
    }


    @Override
    public Set<Student> read() throws IOException, InvalidCsvFormatException {
        Set<Student> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(FILE_NAME);

        while (reader.hasNextLine()) {
            Parcours parcours = null;
            String[] parcoursInfos = reader.nextLine().split(",");
            Student toAdd = null;

            if (parcoursInfos[3].equals("")) {
                toAdd = new Student(parcoursInfos[0], parcoursInfos[1], parcoursInfos[2]);
            } else {
                for (Parcours p : this.parcoursSet) {
                    if (p.getName().equals(parcoursInfos[3])) {
                        parcours = p;
                        break;
                    }
                }
                if (parcours == null) {
                    throw new InvalidCsvFormatException("Parcours \"" + parcoursInfos[3] + "\" doesn't exist !");
                } else {
                    toAdd = new Student(parcoursInfos[0], parcoursInfos[1], parcoursInfos[2], parcours);
                }
            }

            result.add(toAdd);

            if (!parcoursInfos[4].equals("")) {
                for (String euTracking : parcoursInfos[4].split(";")) {
                    String[] ueInfos = euTracking.split(":");
                    for (UE ue : this.ue) {
                        if (ue.getId().equals(ueInfos[0])) {
                            for (Semester s : this.semesters) {
                                if (s.getId().equals(ueInfos[2])) {
                                    try {
                                        toAdd.addUE(ue, s);
                                    } catch (Exception e) {
                                        throw new InvalidCsvFormatException("Error while retrieving data from csv : " + e.toString());
                                    }
                                    break;
                                }
                            }
                            switch (ueInfos[1]) {
                                case "V":
                                    toAdd.validateUE(ue);
                                    break;
                                case "O":
                                    break;
                                case "F":
                                    toAdd.failUE(ue);
                                    break;
                                default:
                                    throw new InvalidCsvFormatException("Unvalid state in csv");
                            }
                            break;
                        }
                    }
                }
            }
        }
        reader.close();
        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();


        for (Student student : university.getStudents()) {
            toWrite.append(student.getId()).append(',').append(student.getFstName()).append(',').append(student.getLstName()).append(',').append(student.getParcours().getName()).append(',');
            for (UETracking ueTracking : student.getUeTracking()) {
                char state = ueTracking.getState() == StateEnum.VALIDATED ? 'V' : ueTracking.getState() == StateEnum.FAILED ? 'F' : 'O';
                toWrite.append(ueTracking.getUe().getId()).append(':').append(state).append(':').append(ueTracking.getSemester().getId()).append(';');
            }
            toWrite.append('\n');
        }
        toWrite.deleteCharAt(toWrite.length() - 1);

        writer.write(toWrite.toString());
        writer.close();
    }
}