package parser;

import model.*;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Class used to parse User in users.csv file
 * format :
 *  userName,password,userType
 *  with user type as:
 *      D = Director
 *      S = Secretary
 */
public class UserParser implements ICsvParser {
    public static final String FILE_NAME = "users.csv";

    public Set<User> read() throws IOException, InvalidCsvFormatException {
        Set<User> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(UserParser.FILE_NAME);

        while (reader.hasNextLine()) {
            String[] line = reader.nextLine().split(",");
            if (line[2].equalsIgnoreCase("D")) {
                result.add(new Director(line[0], line[1]));
            } else if (line[2].equalsIgnoreCase("S")) {
                result.add(new Secretary(line[0], line[1]));
            } else {
                throw new InvalidCsvFormatException("Invalid user type !");
            }
        }
        reader.close();

        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();


        for (User user : university.getUsers()) {
            toWrite.append(user.getId()).append(",").append(user.getPassword()).append(",").append(user instanceof  Director ? 'D' : 'S').append("\n");
        }
        toWrite.deleteCharAt(toWrite.length() - 1);

        writer.write(toWrite.toString());
        writer.close();
    }
}
