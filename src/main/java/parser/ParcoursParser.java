package parser;

import model.Mention;
import model.Parcours;
import model.University;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Class used to parse Parcours in parcours.csv file
 * format :
 *  parcoursName,mentionName
 */
public class ParcoursParser implements ICsvParser {
    public static final String FILE_NAME = "parcours.csv";
    private Set<Mention> mentions;

    public ParcoursParser(Set<Mention> mentions) {
        this.mentions = mentions;
    }

    @Override
    public Set<Parcours> read() throws IOException, InvalidCsvFormatException {
        Set<Parcours> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(FILE_NAME);
        Mention mention = null;

        while (reader.hasNextLine()) {
            String[] parcoursInfos = reader.nextLine().split(",");
            if (parcoursInfos.length != 2) {
                throw new InvalidCsvFormatException("Parcours file is corrupted !");
            }
            for (Mention m : this.mentions) {
                if (m.getName().equals(parcoursInfos[1])) {
                    mention = m;
                }
            }
            if (!parcoursInfos[1].equals("") && mention == null) {
                throw new InvalidCsvFormatException("Mention \"" + parcoursInfos[1] + "\n doesn't exist !");
            }
            result.add(new Parcours(parcoursInfos[0], mention));
        }
        reader.close();
        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();


        for (Parcours parcours : university.getParcours()) {
            toWrite.append(parcours.getName()).append(',').append(parcours.getMention().getName()).append('\n');
        }
        toWrite.deleteCharAt(toWrite.length() - 1);

        writer.write(toWrite.toString());
        writer.close();
    }
}
