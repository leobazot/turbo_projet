package parser;

import model.*;
import model.exceptions.NullUEException;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

/**
 * Class used to parse Parcours in parcours.csv file
 * format :
 *  id,DateBegin,DateEnd
 * with dates on format dd/MM/yyyy
 */
public class SemesterParser implements ICsvParser {
    public static final String FILE_NAME = "semesters.csv";

    @Override
    public Set<Semester> read() throws IOException, InvalidCsvFormatException, NullUEException {
        Set<Semester> result = new HashSet<>();
        Scanner reader = CsvParser.getReader(FILE_NAME);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


        while (reader.hasNextLine()) {
            String[] line = reader.nextLine().split(",");
            try {
                if (Integer.parseInt(line[1].split("/")[1]) >= 7) {
                    result.add(new OddSemester(UUID.fromString(line[0]),dateFormat.parse(line[1]), dateFormat.parse(line[2])));
                } else {
                    result.add(new EvenSemester(UUID.fromString(line[0]),dateFormat.parse(line[1]), dateFormat.parse(line[2])));
                }
            } catch (ParseException e) {
                throw new InvalidCsvFormatException("Date parsing impossible, database must have been corrupted !");
            }
        }
        return result;
    }

    @Override
    public void write() throws IOException {
        University university = University.getUniversity();
        FileWriter writer = CsvParser.getWriter(FILE_NAME);
        StringBuilder toWrite = new StringBuilder();

        boolean addCurentYear = true;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


        for (UnivYear year : university.getUnivYears()) {
            toWrite.append(year.getBegin().getId()).append(',').append(dateFormat.format(year.getBegin().getBegining())).append(',').append(dateFormat.format(year.getBegin().getEnd())).append('\n');
            toWrite.append(year.getEnd().getId()).append(',').append(dateFormat.format(year.getEnd().getBegining())).append(',').append(dateFormat.format(year.getEnd().getEnd())).append('\n');
            if (year.equals(university.getCurrentYear())) {
                addCurentYear = false;
            }
        }
        if (addCurentYear) {
            UnivYear year = university.getCurrentYear();
            toWrite.append(year.getBegin().getId()).append(',').append(dateFormat.format(year.getBegin().getBegining())).append(',').append(dateFormat.format(year.getBegin().getEnd())).append('\n');
            toWrite.append(year.getEnd().getId()).append(',').append(dateFormat.format(year.getEnd().getBegining())).append(',').append(dateFormat.format(year.getEnd().getEnd()));
        } else {
            toWrite.deleteCharAt(toWrite.length() - 1);
        }

        writer.write(toWrite.toString());
        writer.close();
    }
}
