package parser.exceptions;

public class InvalidCsvFormatException extends Exception {
    private String reason;
    public InvalidCsvFormatException(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "An error occured while trying to read one of the csv files : " + reason;
    }
}
