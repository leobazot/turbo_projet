package parser;

import model.User;
import model.exceptions.NullUEException;
import parser.exceptions.InvalidCsvFormatException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

public interface ICsvParser {
    Set<?> read() throws IOException, InvalidCsvFormatException, NullUEException;
    void write() throws IOException;
}
