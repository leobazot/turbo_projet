/**
 * Provide the classes needed to parse save file on csv format using opencsv (see {@link com.opencsv}library
 *
 * @author Léo Bazot <leo.bazot@protonmail.com>
 * @since 0.01
 * @see com.opencsv
 */
package parser;