package view;

import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;


/**
 * JavaFX App
 */
public class App extends Application {
	public static Stage pageAct;
	

    @Override
    public void start(Stage stage) {
        //var javaVersion = SystemInfo.javaVersion();
        //var javafxVersion = SystemInfo.javafxVersion();
    	pageAct=stage;

		try {
			Parent login = FXMLLoader.load(this.getClass().getResource("/Login.fxml")); //Pour tester les autres fichiers, remplacer ModifEtudiant par Login ou Admin
			var scene = new Scene(login,1530,790);
			pageAct.setScene(scene);
			pageAct.show();
			
			
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static Stage getScene() {
		return pageAct;
    }

    public static void main(String[] args) {
        launch(args);
    }

}