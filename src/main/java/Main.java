import model.University;
import model.tracking.UETracking;
import model.Student;
import parser.CsvParser;
import view.App;

public class Main {
    public static void main(String[] args) {
        CsvParser.ReadAll();
        System.out.println("CURRENT YEAR : " + University.getUniversity().getCurrentYear());
        for (Student s : University.getUniversity().getStudents()) {
            System.out.println(s.getFstName() + " :");
            for (UETracking ue : s.getUeTracking()) {
                System.out.println("\t" + ue.getUe().getId() + "\n\t\t" + ue.getSemester() + "\n\t\t" + ue.getYear());
            }
        }
        App.main(args);
        CsvParser.saveAll();
    }
}
