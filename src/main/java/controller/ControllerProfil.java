package controller;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import model.Director;
import model.Parcours;
import model.Student;
import model.University;
import model.exceptions.StudentNotFoundException;
import model.tracking.FailedState;
import model.tracking.StateEnum;
import model.tracking.UETracking;
import model.tracking.ValidatedState;
import view.App;

/**
 * controller of the fxml file ProfilEtudiant
 * @author Liam Buand
 *
 */
public class ControllerProfil implements Initializable{
	
	@FXML
    private TableView<DescriptionLigne> Table_infosUE;
	
	@FXML
    private TableColumn<DescriptionLigne, HBox> colAccept;

    @FXML
    private TableColumn<DescriptionLigne, String> colEcts;

    @FXML
    private TableColumn<DescriptionLigne, String> colUE;

    @FXML
    private TableColumn<DescriptionLigne, String> colValid;

	@FXML
    private Label hyperlien_Modif;
	
	@FXML
    private Label label_id;

    @FXML
    private Label label_nom;

	@FXML
    private Label label_prenom;
	
	@FXML
    private Label labelLogo;
	
	@FXML
	private ComboBox<String> comboAnnee;
	
	private String nom;
	private String id;
	private String prenom;
	private Parcours parcours;
	
	
	/**
	 * creates a controller for the profile page taking a student's first and last name, id and cursus
	 * @param nom
	 * @param prenom
	 * @param id
	 * @param parcours
	 */
	public ControllerProfil(String nom, String prenom, String id, Parcours parcours) {
		this.nom=nom;
		this.prenom=prenom;
		this.id=id;
		this.parcours=parcours;
	}

	/**
	 * navigates from the profile page to the modification page
	 * @param event is a mouseclick on the modifier text
	 */
	@FXML
	void GoToModif(MouseEvent event) {
		try {
			FXMLLoader suite = new FXMLLoader(this.getClass().getResource("/ModifEtudiant.fxml"));
			ControllerModif contr = new ControllerModif(this.nom, this.prenom, this.id, this.parcours);
			suite.setController(contr);
			Parent modif = suite.load();
			Scene modifEtu = new Scene(modif, 1530, 790);
			App.getScene().setScene(modifEtu);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * when clicking on the logo brings the user back to the home page Admin
	 * @param event
	 */
	@FXML
    void backToHome(MouseEvent event) {
		try {
			Parent suite = FXMLLoader.load(this.getClass().getResource("/Admin.fxml"));
			Scene accueil = new Scene(suite, 1530, 790);
			App.getScene().setScene(accueil);
		} catch (IOException e) {
			System.out.println("erreur retour");
		}
    }
	
	/**
	 * returns the selected year in the combobox
	 * @return a year in the String format
	 */
	public String getAnneeCombo () {
		return this.comboAnnee.getSelectionModel().getSelectedItem();
	}
	
	/**
	 * provides adequate text to the labels to fit the Student's profile name
	 * launches the initialisation of the combobox and fills up the TableView
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.label_id.setText(this.id);
		this.label_nom.setText(this.nom);
		this.label_prenom.setText(this.prenom);
		this.fillCombo();
		this.comboAnnee.getSelectionModel().selectFirst();
		this.initTab();
	}
	
	/**
	 * fills the combobox with years of obtained of followed UEs
	 */
	public void fillCombo() {
		Student eleve=null;
		//this.comboAnnee.getItems().add("2001/2002");
		try {
			eleve = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
		} catch (StudentNotFoundException e) {}
		Set<UETracking> valide = eleve.getValidateddUETracking();
		Set<UETracking> suivie = eleve.getFollowedUETracking();
		Set<UETracking> manque = eleve.getFailedUETracking();
		valide.addAll(manque);
		valide.addAll(suivie);
		for (UETracking ue : valide) {
			String annee = ue.getYear().toString();
			boolean existe=false;
			for (String s : this.comboAnnee.getItems()) {
				if (annee.equals(s)) {
					existe=true;
				}
			}
			if (!existe) {
				this.comboAnnee.getItems().add(annee);
			}
		}
	}
	
	/**
	 * fills up the grid from the student's information at the start of the page with a specific year (combobox)
	 */
	@FXML
	private void initTab() {
		Student eleve=null;
		try {
			eleve = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
		} catch (StudentNotFoundException e) {}
		Set<UETracking> valide = new HashSet<UETracking>();
		valide = eleve.getValidateddUETracking();
		Set<UETracking> suivie = eleve.getFollowedUETracking();
		Set<UETracking> manque = eleve.getFailedUETracking();
		valide.addAll(manque);
		ObservableList<DescriptionLigne> contenu = FXCollections.observableArrayList();
		for (UETracking uev : valide) {
			boolean doublon=false;
			for (DescriptionLigne dl : contenu) {
				if (dl.getNomUE().equals(uev.getUe().getId())) {
					doublon=true;
				}
			}
			if (!doublon && uev.getYear().toString().equals(this.getAnneeCombo())) {
				DescriptionLigne ligne;
				if (uev.getState().equals(StateEnum.FAILED)) {
					ligne = new DescriptionLigne(uev.getUe().getId(), "non", "0/"+uev.getUe().getEcts());
				}
				else {
					int nbEcts = uev.getUe().getEcts();
					ligne = new DescriptionLigne(uev.getUe().getId(), "oui", nbEcts+"/"+nbEcts);
				}
				contenu.add(ligne);
			}
		}
		if (this.getAnneeCombo().equals(University.getUniversity().getCurrentYear().toString())) {
			for (UETracking uesuiv : suivie) {
				boolean doublon=false;
				for (DescriptionLigne dl : contenu) {
					if (dl.getNomUE().equals(uesuiv.getUe().getId())) {
						doublon=true;
					}
				}
				if (!doublon && uesuiv.getYear().toString().equals(this.getAnneeCombo())) {
					DescriptionLigne ligne = new DescriptionLigne(uesuiv.getUe().getId(), "en cours", "0/"+uesuiv.getUe().getEcts());
					contenu.add(ligne);
				}
			}
		}
		
		this.colUE.setCellValueFactory(new PropertyValueFactory<DescriptionLigne, String>("nomUE"));
		this.colValid.setCellValueFactory(new PropertyValueFactory<DescriptionLigne, String>("valide"));
		this.colEcts.setCellValueFactory(new PropertyValueFactory<DescriptionLigne, String>("ects"));
		this.colAccept.setCellValueFactory(new PropertyValueFactory<DescriptionLigne, HBox>("box"));
		this.Table_infosUE.setItems(contenu);
		
	}
	
	
	/**
	 * when clicking on ✔ button validates the UE for the student
	 * @param event
	 */
	public void validation(MouseEvent event) {
		Student eleve=null;
		try {
			eleve = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
		} catch (StudentNotFoundException e) {}
		UETracking uec = null;
		for (UETracking ue : eleve.getFollowedUETracking()) {
			if (ue.getUe().getId().equals(this.Table_infosUE.getSelectionModel().getSelectedItem().getNomUE()))
				uec = ue;
		}
		int indice = this.Table_infosUE.getItems().indexOf(this.Table_infosUE.getSelectionModel().getSelectedItem());
		ValidatedState valid = new ValidatedState();
		uec.changeState(valid);
		this.Table_infosUE.getItems().get(indice).setValide("oui");
		this.initTab();
	}
	
	/**
	 * when clicking on ❌ button makes the student fail the UE
	 * @param event
	 */
	public void refus(MouseEvent event) {
		Student eleve=null;
		try {
			eleve = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
		} catch (StudentNotFoundException e) {}
		UETracking uec = null;
		for (UETracking ue : eleve.getFollowedUETracking()) {
			if (ue.getUe().getId().equals(this.Table_infosUE.getSelectionModel().getSelectedItem().getNomUE()))
				uec = ue;
		}
		int indice = this.Table_infosUE.getItems().indexOf(this.Table_infosUE.getSelectionModel().getSelectedItem());
		FailedState failed = new FailedState();
		uec.changeState(failed);
		this.Table_infosUE.getItems().get(indice).setValide("non");
		this.initTab();
	}

	/**
	 * class used to describe a row in the grid for easier insertion
	 * @author Liam Buand
	 *
	 */
	public class DescriptionLigne {
		String nomUE;
		String valide;
		String ects;
		HBox box;
		
		/**
		 * Takes the ue's name, state of validity and ects given to the student to create a row element
		 * @param annee
		 * @param valide
		 * @param ects
		 */
		public DescriptionLigne(String nomUE, String valide,
				String ects) {
			this.nomUE=nomUE;
			this.valide=valide;
			this.ects=ects;
			Button validerUE = new Button("✔");
			validerUE.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {
					validation(arg0);
				}
			});
			Button refuserUE = new Button("❌");
			refuserUE.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {
					refus(arg0);
				}
			});
			this.box = new HBox(validerUE, refuserUE);
			if (!(University.getUniversity().getConnected() instanceof Director)) {
	    		validerUE.setDisable(true);
	    		refuserUE.setDisable(true);
	    	}
		}

		public String getNomUE() {
			return this.nomUE;
		}

		public String getEcts() {
			return this.ects;
		}

		public String getValide() {
			return this.valide;
		}
		
		public void setValide(String str) {
			this.valide=str;
		}
		
		public HBox getBox() {
			return this.box;
		}
	}

	

}
