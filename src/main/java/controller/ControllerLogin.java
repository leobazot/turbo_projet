package controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import model.University;
import model.exceptions.UserLogInException;
import view.App;

/**
 * controller related to the Login fxml file
 * @author Liam Buand
 *
 */
public class ControllerLogin {
	

    @FXML
    private Button BtnCo;

    @FXML
    private Label ErrorMsgConnexion;

    @FXML
    private TextField IdConnexion;

    @FXML
    private PasswordField MdpConnexion;

    /**
     * checks if the info given is legit and if so navigates to the homepage else renders an error message
     * @param event
     */
    @FXML
    public void seConnecter(InputEvent event) {
    	KeyEvent k = null;
    	if (event instanceof KeyEvent) {
    		k = (KeyEvent) event;
    	}
    	if (event instanceof MouseEvent || k.getCode().equals(KeyCode.ENTER)) {
    		University u = University.getUniversity();
    		try {
    			u.logIn(this.IdConnexion.getText(), this.MdpConnexion.getText());
    			try {
    				Parent suite = FXMLLoader.load(this.getClass().getResource("/Admin.fxml"));
    				Scene accueil = new Scene(suite, 1530, 790);
    				App.getScene().setScene(accueil);
    			} catch (IOException e) {}
    			
    		} catch (UserLogInException e) {
    			this.ErrorMsgConnexion.setText(e.toString());
    			this.ErrorMsgConnexion.setVisible(true);
    		}	
    	}
    }

}
