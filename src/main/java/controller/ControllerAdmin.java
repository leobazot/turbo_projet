package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.Director;
import model.Mention;
import model.Parcours;
import model.Student;
import model.University;
import model.exceptions.NullParcoursException;
import view.App;


/**
 * Controller of the fxml page dedicated to the homepage Admin
 * @author Liam Buand
 *
 */
public class ControllerAdmin implements Initializable{
	
	@FXML
    private TableView<DescriptionEtudiant> TableEtu;

    @FXML
    private TableColumn<DescriptionEtudiant, String> colMention;

    @FXML
    private TableColumn<DescriptionEtudiant, Button> colModif;

    @FXML
    private TableColumn<DescriptionEtudiant, String> colNom;

    @FXML
    private TableColumn<DescriptionEtudiant, String> colNum;

    @FXML
    private TableColumn<DescriptionEtudiant, String> colParcours;

    @FXML
    private TableColumn<DescriptionEtudiant, String> colPrenom;

    @FXML
    private Button Btn_AjoutEtud;

    @FXML
    private Label label_annee;
    
    /**
     * navigates from Admin to ModifEtudiant when clicking the ajouter button (the page will be empty)
     * @param event is the mouseclick on the button ajouter
     */
    @FXML
    public void ajoutEtudiant(MouseEvent event) {
    	try {
			FXMLLoader suite = new FXMLLoader(this.getClass().getResource("/ModifEtudiant.fxml"));
			ControllerModif contr = new ControllerModif(null, null, null, null);
			suite.setController(contr);
			Parent modif = suite.load();
			Scene ajoutEtu = new Scene(modif, 1530, 790);
			App.getScene().setScene(ajoutEtu);
		} catch (IOException e) {}
    }
    
    /**
     * enables the modification controller to access the details of the selected student
     * @return the student of the selected row
     */
    public Student getSelectedStudent() {
    	DescriptionEtudiant etu = this.TableEtu.getSelectionModel().getSelectedItem();
    	Student s = new Student(etu.getId(), etu.getPrenom(), etu.getNom());
    	try {
			s.setParcours(new Parcours(etu.getParcours(), new Mention(etu.getMention())));
		} catch (NullParcoursException e) {
			e.printStackTrace();
		}
    	return s;
    }
    
    
    /**
     * navigates from the page Admin to ModifEtudiant where the student's information will already be entered
     * @param event is the mouseclick on '...'
     */
    @FXML
    public void toPageModifEtudiant(MouseEvent event) {
    	try {
			FXMLLoader suite = new FXMLLoader(this.getClass().getResource("/ModifEtudiant.fxml"));
	    	Student etuAct=this.getSelectedStudent();
			ControllerModif contr = new ControllerModif(etuAct.getLstName(), etuAct.getFstName(), etuAct.getId(), etuAct.getParcours());
			suite.setController(contr);
			Parent modif = suite.load();
			Scene ajoutEtu = new Scene(modif, 1530, 790);
			App.getScene().setScene(ajoutEtu);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * navigates to the profile page of the highlighted student
     * @param event has to be a double-click to do something
     */
    @FXML
    void accesProfil(MouseEvent event) {
    	if (event.getClickCount()==2 && this.getSelectedStudent()!=null) {
    		try {
    			FXMLLoader suite = new FXMLLoader(this.getClass().getResource("/ProfilEtudiant.fxml"));
    	    	Student etuAct=this.getSelectedStudent();
    			ControllerProfil contr = new ControllerProfil(etuAct.getLstName(), etuAct.getFstName(), etuAct.getId(), etuAct.getParcours());
    			suite.setController(contr);
    			Parent modif = suite.load();
    			Scene ajoutEtu = new Scene(modif, 1530, 790);
    			App.getScene().setScene(ajoutEtu);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    /**
     * render certain buttons only if the user is of type admin, and will also place the correct date in the corresponding label
     * @since 1.0
     * 
     */
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
    	this.label_annee.setText(University.getUniversity().getCurrentYear().toString());
    	if (!(University.getUniversity().getConnected() instanceof Director)) {
    		this.Btn_AjoutEtud.setDisable(true);
    		this.colModif.setVisible(false);
    	}
    	this.initTab();
    	this.TableEtu.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	}
    
    
    /**
     * fills the table with students
     */
    private void initTab() {
    	this.TableEtu.getItems().clear();
    	ObservableList<DescriptionEtudiant> contenu = FXCollections.observableArrayList();
		Set<Student> listEtu = University.getUniversity().getStudents();
		for (Student s : listEtu) {
			String id, nom, prenom, parcours, mention;
			id=s.getId();
			nom=s.getLstName();
			prenom=s.getFstName();
			if (s.getParcours()==null) {
				parcours="";
				mention="";
			}
			else {
				parcours=s.getParcours().getName();
				mention=s.getParcours().getMention().getName();				
			}
			DescriptionEtudiant ligne = new DescriptionEtudiant(id, nom, prenom, mention, parcours);
			contenu.add(ligne);
		}
		this.colNum.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, String>("id"));
		this.colNom.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, String>("nom"));
		this.colPrenom.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, String>("prenom"));
		this.colMention.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, String>("mention"));
		this.colParcours.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, String>("parcours"));
		this.colModif.setCellValueFactory(new PropertyValueFactory<DescriptionEtudiant, Button>("modif"));
		this.TableEtu.setItems(contenu);
		
	}

	/**
     * class which describes the content of the table
     * @author Liam Buand
     *
     */
    public class DescriptionEtudiant{
    	String id;
    	String nom;
    	String prenom;
    	String mention;
    	String parcours;
    	Button modif;
		public DescriptionEtudiant(String id, String nom, String prenom, String mention, String parcours) {
			this.id = id;
			this.nom = nom;
			this.prenom = prenom;
			this.mention = mention;
			this.parcours = parcours;
			this.modif = new Button("...");
			this.modif.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {
					toPageModifEtudiant(arg0);
				}
			});
		}
		public String getId() {
			return id;
		}
		public String getNom() {
			return nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public String getMention() {
			return mention;
		}
		public String getParcours() {
			return parcours;
		}
		public Button getModif() {
			return modif;
		}
    	
    	
    }

	

}
