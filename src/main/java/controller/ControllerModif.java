package controller;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import model.Parcours;
import model.Student;
import model.UE;
import model.UEMention;
import model.UEOuverture;
import model.University;
import model.exceptions.MissingPrerequesiteException;
import model.exceptions.NullParcoursException;
import model.exceptions.NullStudentException;
import model.exceptions.StudentNotFoundException;
import model.exceptions.WrongParcoursException;
import model.tracking.UETracking;
import view.App;

/**
 * controller of the fxml file ModifEtudiant
 * @author Liam Buand
 *
 */
public class ControllerModif implements Initializable{

    @FXML
    private TreeView<UE> AffListeModif;

    @FXML
    private TextField IdModif;

    @FXML
    private Label MsgLicenceModif;

    @FXML
    private TextField NomModif;

    @FXML
    private TextField PrenomModif;

    @FXML
    private TextField RechercheModif;

    @FXML
    private Button ValiderModif;
    
    @FXML
    private Button AnnulerModif;
    
    @FXML
    private Button BtnRetour;
    
    @FXML
    private StackPane PlaceholderPane;
    
    @FXML
    private ComboBox<Parcours> comboParcours;
    
    private Set<Parcours> listeParcours = University.getUniversity().getParcours();
    
    private String nom;
    private String prenom; 
    private String id;
    private Parcours licence;
    
    /**
     * enables to create the controller of the modification page with the informations of th student to modify
     * @param nom
     * @param prenom
     * @param id
     * @param parcours
     */
    public ControllerModif(String nom, String prenom, String id, Parcours parcours) {
    	this.nom=nom;
    	this.prenom=prenom;
    	this.id=id;
    	this.licence=parcours;
    }

    /**
     * hide nodes in the display of UE not corresponding to the text of the research
     * doesn't work yet
     * @param event is a mouseclick on the magnifying glass button or an enter on the search bar
     */
    @FXML
    void rechercheUE(InputEvent event) {
    	KeyEvent k = null;
    	if (event instanceof KeyEvent) {
    		k = (KeyEvent) event;
    	}
    	if (event instanceof MouseEvent || k.getCode().equals(KeyCode.ENTER)) {
    		for (TreeItem<UE> t : this.AffListeModif.getRoot().getChildren()) {
    			if (!t.getValue().getId().contains(this.RechercheModif.getText())) {
    				//rendre le noeud t invisible?
    			}
        	}
    	}
    	if (this.AffListeModif.getRoot().getChildren().isEmpty()) {
    		this.PlaceholderPane.setVisible(true);
    	}
    	else {
    		this.PlaceholderPane.setVisible(false);
    	}
    }

    /**
     * resets all fields to empty
     * @param event is the mouseclick on annuler button
     */
    @FXML
    void resetChamps(MouseEvent event) {
    	this.NomModif.setText("");
    	this.PrenomModif.setText("");
    	this.IdModif.setText("");
    }

    /**
     * saves any changes that will be rewritten in the csv file
     * @param event is a mouseclick on Valider
     */
    @FXML
    void validChangement(MouseEvent event) {
    	Student etu = null;
    	Parcours parcours = this.comboParcours.getValue();
    	if (this.nom!=null) {
    		try {
    			etu = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
    			etu.setFstName(this.PrenomModif.getText());
    			etu.setLstName(this.NomModif.getText());
    			etu.setId(this.IdModif.getText());
				if (parcours != null) {
					etu.setParcours(parcours);
				}
    		} catch (StudentNotFoundException e) {
    			
    		} catch (NullParcoursException e) {
				e.printStackTrace();
			}
    	}
    	else {
    		try {
    			University.getUniversity().addStudent(new Student(this.IdModif.getText(), this.PrenomModif.getText(), this.NomModif.getText(), parcours));
    			etu = University.getUniversity().searchStudent(this.NomModif.getText()+" "+this.PrenomModif.getText()).get(0);
    		} catch (NullStudentException e1) {
    			e1.printStackTrace();
    		} catch (StudentNotFoundException e) {}
    	}
    	List<UE> UEChoisie = this.obtenirUE();
    	for (UE ue : UEChoisie) {
    		try {
				etu.addUE(ue);
			} catch (MissingPrerequesiteException | WrongParcoursException e) {
				e.printStackTrace();
			}    		
    	}
    	this.retourAccueil(null);
    	
    }
    
    /**
     * brings the user back to the home page by clicking the retour accueil button or byt the call of a method
     * @param event
     */
    @FXML
    void retourAccueil(MouseEvent event) {
    	try {
    		Parent suite = FXMLLoader.load(getClass().getResource("/Admin.fxml"));
    		Scene accueil = new Scene(suite, 1530, 790);
    		App.getScene().setScene(accueil);
    	}catch (IOException e) {
   			e.printStackTrace();
    	}
    }
    
    /**
     * returns the selected UEs from the display of UEs
     * @return
     */
    private List<UE> obtenirUE() {
        return this.AffListeModif.getSelectionModel().getSelectedItems()
               .stream()
               .map(TreeItem::getValue)
               .collect(Collectors.toList());
    }
    
    /**
     * called at the start of the page, fills the fields with a Students information if they are given (modification)
     * calls the method to display UEs
     */
    @Override
    public void initialize (URL url, ResourceBundle rb) {
    	this.comboParcours.getSelectionModel().selectFirst();
    	this.NomModif.setText(this.nom);
    	this.PrenomModif.setText(this.prenom);
    	this.IdModif.setText(this.id);
    	if (this.licence!=null) {    		
    		this.MsgLicenceModif.setText("Actuellement en "+this.licence.toString());
    	}
    	this.chargerUE();
    	this.chargerParcours();
    	this.AffListeModif.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * initializes the combobox with existing cursus
     */
    private void chargerParcours() {
    	ObservableList<Parcours> o = FXCollections.observableArrayList();
    	o.addAll(listeParcours);
    	this.comboParcours.setItems(o);
	}

	/**
     * loads all UE in the treeview display at the start of the page ignoring UE validated or followed
     * by a student
     */
	private void chargerUE() {
		TreeItem<UE> root = new TreeItem<>(null);
		Set<UE> listeUE = University.getUniversity().getUE();
		if (this.nom==null) {
			for (UE ue : listeUE) {
				if (ue instanceof UEOuverture) {
					boolean doublon=false;
					for (TreeItem<UE> t : root.getChildren()) {
						if (t.getValue().getId().equals(ue.getId())) {
							doublon=true;
						}
					}
					if (!doublon) {
						TreeItem<UE> ueouv = new TreeItem<>(ue);
						root.getChildren().add(ueouv);					
					}
				}
			}
		}
		else {
			Set<UE> suivable = new HashSet<>();
			Student eleve = null;
			try {
				eleve = University.getUniversity().searchStudent(this.nom+" "+this.prenom).get(0);
				suivable = eleve.getFollowableUE();
			} catch (StudentNotFoundException e) {
				e.printStackTrace();
			}
			Set<UETracking> valide = eleve.getValidateddUETracking();
			Set<UETracking> suivie = eleve.getFollowedUETracking();
			valide.addAll(suivie);
			for (UETracking dejaValid : valide) {
				if (suivable.contains(dejaValid.getUe())) {
					suivable.remove(dejaValid.getUe());
				}
			}
			for (UE ue : suivable) {
				if (ue instanceof UEMention) {
					boolean doublon=false;
					TreeItem<UE> parent=null;
					for (TreeItem<UE> t : root.getChildren()) {
						if (t.getValue().getId().equals(ue.getMention().getName())) {
							parent=t;
						}
					}
					if (parent==null) {
						TreeItem<UE> mentionAff = new TreeItem<>(new UEOuverture(ue.getMention().getName(), 0));
						parent= mentionAff;
						root.getChildren().add(parent);
					}
					for (TreeItem<UE> f : parent.getChildren()) {
						if (f.getValue().getId().equals(ue.getId())) {
							doublon=true;
						}
					}
					if (!doublon) {
						TreeItem<UE> uemention = new TreeItem<>(ue);
						parent.getChildren().add(uemention);
					}
				}
				else if (ue instanceof UEOuverture) {
					boolean doublon=false;
					for (TreeItem<UE> t : root.getChildren()) {
						if (t.getValue().equals(ue.getId())) {
							doublon=true;
						}
					}
					if (!doublon) {
						TreeItem<UE> ueouv = new TreeItem<>(ue);
						root.getChildren().add(ueouv);					
					}
				}
			}
		}
		this.AffListeModif.setRoot(root);
		this.AffListeModif.setShowRoot(false);
		if (root.getChildren().isEmpty()) {
			this.PlaceholderPane.setVisible(true);
		}
		else {
			this.PlaceholderPane.setVisible(false);
		}
	}

}
